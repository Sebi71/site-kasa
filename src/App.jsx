/**
 * This App component represents the basic structure of the application, 
 * including navigation, application content, and footer.
 */

//Import of HashRouter which allows you to encapsulate the entire application.
import { HashRouter} from "react-router-dom";
//Import the navbar and footer components, and AppRoutes which contains the paths.
import NavBar from "./components/NavBar";
import Footer from "./components/Footer";
// import AppRoutes from "./routes/AppRoutes";
import { Routes, Route } from "react-router-dom";
// Importing application pages
import Home from "../../pages/Home"
import About from "../../pages/About";
import Housing from "../../pages/Housing";
import NotFound from "../../pages/NotFound";


export default function App() {
    return (
        <>
            <HashRouter>
                <NavBar />
                <Routes>
                    {/* path to Home page */}
                    <Route path="/" element={<Home />} />
                    {/* path to About page */}
                    <Route path="/about" element={<About />} />
                    {/* path to Housing page with the retrieved ID*/}
                    <Route path="/housing/:id" element={<Housing />} />
                    {/* path to Error page */}
                    <Route path="*" element={<NotFound />} />
                </Routes>
                <Footer/>
            </HashRouter>
        </>
    )
}